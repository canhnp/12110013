﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Comment
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        public String Author { set; get; }
        //
        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Days * 60 * 24 + (DateTime.Now - DateCreated).Hours * 60 + (DateTime.Now - DateCreated).Minutes;
                //return (DateTime.Now - DateCreated).Minutes;
            }
        }
        //
        public virtual Post Post { set; get; }
        public int PostID { set; get; }
    }
}