﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(500, ErrorMessage = "Số lượng ký tự trong khoảng 20 - 500", MinimumLength = 20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        //
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
        //
        public virtual ICollection<Tag> Tags { set; get; }
    }
}