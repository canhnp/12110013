namespace FinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Comments", "Author", c => c.String(nullable: true));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Comments", "Author", c => c.String());
        }
    }
}
