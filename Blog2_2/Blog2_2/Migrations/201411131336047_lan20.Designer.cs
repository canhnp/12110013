// <auto-generated />
namespace Blog2_2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan20 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan20));
        
        string IMigrationMetadata.Id
        {
            get { return "201411131336047_lan20"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
