namespace Blog2_2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountID = c.Int(nullable: false, identity: true),
                        Password = c.String(),
                        Email = c.String(),
                        FirstName = c.String(),
                        LastName = c.String(),
                    })
                .PrimaryKey(t => t.AccountID);
            
            AddColumn("dbo.Post", "AccountID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Post", "AccountID", "dbo.Accounts", "AccountID", cascadeDelete: true);
            CreateIndex("dbo.Post", "AccountID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Post", new[] { "AccountID" });
            DropForeignKey("dbo.Post", "AccountID", "dbo.Accounts");
            DropColumn("dbo.Post", "AccountID");
            DropTable("dbo.Accounts");
        }
    }
}
