// <auto-generated />
namespace Blog2_2.Migrations
{
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    public sealed partial class lan6 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(lan6));
        
        string IMigrationMetadata.Id
        {
            get { return "201410231707068_lan6"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
