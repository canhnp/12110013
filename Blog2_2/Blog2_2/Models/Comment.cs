﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Comment
    {
        [Required(ErrorMessage = "Phải nhập nội dung")]
        public int ID { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        //[RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [Required(ErrorMessage = "Phải nhập đúng")]
        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }

        //[Required(ErrorMessage = "Phải nhập đúng")]
        public String Author { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DateCreated).Days*60*24 + (DateTime.Now - DateCreated).Hours * 60 + (DateTime.Now - DateCreated).Minutes;
            }
        }
        //
        public int PostID { set; get; }
        public virtual Post Post { set; get; }
    }
}