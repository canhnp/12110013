﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    //
    //[Table("Post")]
    //
    public class Post
    {
        [Required(ErrorMessage = "Phải nhập nội dung")]
        //Method(Phuong Thuc)
        public int ID { set; get; }
        [Required(ErrorMessage="Phải nhập nội dung")]
        [StringLength(500,ErrorMessage="Số lượng ký tự trong khoảng 20 - 500",MinimumLength=20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        //[StringLength(250, MinimumLength = 50)]
        [RegularExpression(@"^.{50,}$",ErrorMessage="Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }

        [Required(ErrorMessage = "Phải nhập đúng")]
        [DataType(DataType.DateTime)]
        //[RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])[-/](1[0-2]|0[1-9])[-/][0-9]{4}$",ErrorMessage="Nhap theo kieu mm/dd/yy, VD: 2/25/2014")]
        public DateTime DateCreated { set; get; }
        [Required(ErrorMessage = "Phải nhập đúng")]
        [DataType(DataType.DateTime)]
        //[RegularExpression(@"^(3[01]|[12][0-9]|0[1-9])[-/](1[0-2]|0[1-9])[-/][0-9]{4}$", ErrorMessage = "Nhap theo kieu mm/dd/yy, VD: 2/25/2014")]
        public DateTime DateUpdated { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
        //
        public virtual ICollection<Tag> Tags { set; get; }
        //
        public int AccountID { set; get; }
        public virtual Account Account { set; get; }
    }
}