﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog2_2.Models
{
    public class Account
    {
        [Required(ErrorMessage = "Phải nhập nội dung")]
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        //[DataType(DataType.EmailAddress,ErrorMessage="Phai nhap email hop le,vi du : phongcanhit@gmail.com")]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Phải nhập email hợp lệ, VD:phongcanhit@gmail.com")]
        public String Email { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(100,ErrorMessage="Tối đa 100 ký tự")]
        public String FirstName { set; get; }

        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(100,ErrorMessage="Tối đa 100 ký tự")]
        public String LastName { set; get; }
        //
        public virtual ICollection<Post> Posts { set; get; }
    }
}