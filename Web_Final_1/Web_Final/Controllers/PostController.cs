﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class PostController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Post/

        public ActionResult Index()
        {
            var posts = db.Posts.Include(p => p.UserProfile).Include(p => p.PostType);
            return View(posts.ToList());
        }

        //
        // GET: /Post/Details/5

        public ActionResult Details(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(post);
        }

        //
        // GET: /Post/Create

        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            ViewBag.PostTypeID = new SelectList(db.PostTypes, "ID", "NameType");
            return View();
        }

        //
        // POST: /Post/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Post post, string Content)
        {
            if (ModelState.IsValid)
            {
                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                //Gan id vao cho post
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                       .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                //Tao list cac Tag
                List<Tag> Tags = new List<Tag>();
                //Tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lap cac Tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //Neu co Tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        //neu chua co Tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vao List cac Tag
                    Tags.Add(tagExits);
                }
                //Gan List Tag cho Post
                post.Tags = Tags;

                db.Posts.Add(post);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            //
            ViewBag.PostTypeID = new SelectList(db.PostTypes, "ID", "NameType", post.PostTypeID);
            return View(post);
        }
        //Click vao Tag de hien ra cac Post
        [AllowAnonymous]
        public ActionResult _Search(string searchString)
        {
            var posts = from p in db.Posts select p;
            if (!string.IsNullOrEmpty(searchString))
            {
                posts = posts.Where(y => y.Tags.Any(t => t.Content.Contains(searchString)));
            }
            return View(posts);
        }

        //
        // GET: /Post/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            ViewBag.PostTypeID = new SelectList(db.PostTypes, "ID", "NameType", post.PostTypeID);
            return View(post);
        }

        //
        // POST: /Post/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Post post)
        {
            if (ModelState.IsValid)
            {
                post.DateUpdated = DateTime.Now;
                db.Entry(post).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
                
            return View(post);
        }

        //
        // GET: /Post/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Post post = db.Posts.Find(id);
            if (post == null)
            {
                return HttpNotFound();
            }
            return View(post);
        }

        //
        // POST: /Post/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Post post = db.Posts.Find(id);
            db.Posts.Remove(post);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        // tao hinh
        [HttpPost]
        
        public ActionResult FileUpload(Post post, HttpPostedFileBase file,string Content)
        {
            if (ModelState.IsValid)
            {


                post.DateCreated = DateTime.Now;
                post.DateUpdated = DateTime.Now;
                //Gan id vao cho post
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                       .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                post.UserProfileUserId = userid;

                //Tao list cac Tag
                List<Tag> Tags = new List<Tag>();
                //Tach cac Tag theo dau ,
                string[] TagContent = Content.Split(',');
                //Lap cac Tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    Tag tagExits = null;
                    var ListTag = db.Tags.Where(y => y.Content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        //Neu co Tag roi thi add them post vao
                        tagExits = ListTag.First();
                        tagExits.Posts.Add(post);
                    }
                    else
                    {
                        //neu chua co Tag thi tao moi
                        tagExits = new Tag();
                        tagExits.Content = item;
                        tagExits.Posts = new List<Post>();
                        tagExits.Posts.Add(post);
                    }
                    //add vao List cac Tag
                    Tags.Add(tagExits);
                }
                //Gan List Tag cho Post
                post.Tags = Tags;



                if (file != null)
                {
                    string ImageName = System.IO.Path.GetFileName(file.FileName);
                    long t = 0;
                    string ID = db.Posts
                            .Max(c => c.Picture.Substring(2, c.Picture.Length - 6));
                    try
                    {
                        t = int.Parse(ID);
                    }
                    catch
                    {
                    }
                    t++;
                    ID = "MA" + t.ToString() + ".jpg";

                    string physicalPath = Server.MapPath("~/images/" + ID);

                    file.SaveAs(physicalPath);
                    post.DateCreated = DateTime.Now;
                    post.DateUpdated = DateTime.Now;

                    post.Picture = ID;
                    db.Posts.Add(post);
                    db.SaveChanges();
                }
                else
                {

                    post.DateCreated = DateTime.Now;
                    post.DateUpdated = DateTime.Now;

                    post.Picture = "NH0.jpg";
                    db.Posts.Add(post);
                    db.SaveChanges();
                }
                //Display records
                return RedirectToAction("Details", "PostType", new { id = post.PostTypeID });
            }
            //SF
            ViewBag.PostTypeID = new SelectList(db.PostTypes, "PostTypeID", "NameType", post.PostTypeID);
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", post.UserProfileUserId);
            return View(post);
        }
        //------------------------------------------------------
    }
}