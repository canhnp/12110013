﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class PostType
    {
        public int ID { set; get; }

        public String NameType { set; get; }

        public String AdminFlag { set; get; }

        public String RelationshipFlag { set; get; }

        //
        public virtual ICollection<Post> Posts { set; get; }
    }
}