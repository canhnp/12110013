namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan20 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contact1", "EmailNguoiGui", c => c.String());
            AddColumn("dbo.Contact1", "EmailNguoiNhan", c => c.String());
            DropColumn("dbo.Contact1", "Email");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contact1", "Email", c => c.String());
            DropColumn("dbo.Contact1", "EmailNguoiNhan");
            DropColumn("dbo.Contact1", "EmailNguoiGui");
        }
    }
}
