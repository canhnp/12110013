namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "UserProfileID", c => c.Int(nullable: false));
            DropColumn("dbo.Contacts", "PostID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "PostID", c => c.Int(nullable: false));
            DropColumn("dbo.Contacts", "UserProfileID");
        }
    }
}
