namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Tags", "Content", c => c.String(nullable: false, maxLength: 100));
            AlterColumn("dbo.Comments", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.Contact1", "Title", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Contact1", "Body", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Contact1", "Body", c => c.String());
            AlterColumn("dbo.Contact1", "Title", c => c.String());
            AlterColumn("dbo.Comments", "Body", c => c.String());
            AlterColumn("dbo.Tags", "Content", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
        }
    }
}
