namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        Email = c.String(),
                        PostID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Contacts", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Contacts", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Contacts");
        }
    }
}
