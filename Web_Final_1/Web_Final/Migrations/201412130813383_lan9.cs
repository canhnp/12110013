namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan9 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contact1",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        Email = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfile_UserId)
                .Index(t => t.UserProfile_UserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Contact1", new[] { "UserProfile_UserId" });
            DropForeignKey("dbo.Contact1", "UserProfile_UserId", "dbo.UserProfile");
            DropTable("dbo.Contact1");
        }
    }
}
