namespace Blog2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaiViet", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.BaiViet", "Body", c => c.String(maxLength: 250));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaiViet", "Body", c => c.String());
            AlterColumn("dbo.BaiViet", "Title", c => c.String());
        }
    }
}
