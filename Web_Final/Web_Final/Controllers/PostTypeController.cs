﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class PostTypeController : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /PostType/

        public ActionResult Index()
        {
            return View(db.PostTypes.ToList());
        }

        //
        // GET: /PostType/Details/5

        public ActionResult Details(int id = 0)
        {
            PostType posttype = db.PostTypes.Find(id);
            if (posttype == null)
            {
                return HttpNotFound();
            }
            return View(posttype);
        }

        //
        // GET: /PostType/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PostType/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(PostType posttype)
        {
            if (ModelState.IsValid)
            {
                db.PostTypes.Add(posttype);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(posttype);
        }

        //
        // GET: /PostType/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PostType posttype = db.PostTypes.Find(id);
            if (posttype == null)
            {
                return HttpNotFound();
            }
            return View(posttype);
        }

        //
        // POST: /PostType/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(PostType posttype)
        {
            if (ModelState.IsValid)
            {
                db.Entry(posttype).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(posttype);
        }

        //
        // GET: /PostType/Delete/5

        public ActionResult Delete(int id = 0)
        {
            PostType posttype = db.PostTypes.Find(id);
            if (posttype == null)
            {
                return HttpNotFound();
            }
            return View(posttype);
        }

        //
        // POST: /PostType/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PostType posttype = db.PostTypes.Find(id);
            db.PostTypes.Remove(posttype);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}