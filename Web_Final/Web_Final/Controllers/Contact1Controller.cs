﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web_Final.Models;

namespace Web_Final.Controllers
{
    public class Contact1Controller : Controller
    {
        private BlogDbContext db = new BlogDbContext();

        //
        // GET: /Contact1/

        public ActionResult Index()
        {
            return View(db.Contact1.ToList());
        }

        //
        // GET: /Contact1/Details/5

        public ActionResult Details(int id = 0)
        {
            Contact1 contact1 = db.Contact1.Find(id);
            if (contact1 == null)
            {
                return HttpNotFound();
            }
            return View(contact1);
        }

        //
        // GET: /Contact1/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Contact1/Create

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Contact1 contact1)
        {
            if (ModelState.IsValid)
            {
                //Gan id vao cho post
                int userid = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                       .Where(y => y.UserName == User.Identity.Name).Single().UserId;
                contact1.UserProfileID = userid;
                //
                contact1.EmailNguoiNhan="canhnp@gmail.com";
                //
                db.Contact1.Add(contact1);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(contact1);
        }

        //
        // GET: /Contact1/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Contact1 contact1 = db.Contact1.Find(id);
            if (contact1 == null)
            {
                return HttpNotFound();
            }
            return View(contact1);
        }

        //
        // POST: /Contact1/Edit/5

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Contact1 contact1)
        {
            if (ModelState.IsValid)
            {
                db.Entry(contact1).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(contact1);
        }

        //
        // GET: /Contact1/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Contact1 contact1 = db.Contact1.Find(id);
            if (contact1 == null)
            {
                return HttpNotFound();
            }
            return View(contact1);
        }

        //
        // POST: /Contact1/Delete/5

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Contact1 contact1 = db.Contact1.Find(id);
            db.Contact1.Remove(contact1);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}