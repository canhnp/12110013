﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Post
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(50, ErrorMessage = "Số lượng ký tự trong khoảng 10 - 50", MinimumLength = 10)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [RegularExpression(@"^.{30,}$", ErrorMessage = "Nhập tối thiểu 30 ký tự")]
        public String Body { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateCreated { set; get; }

        [DataType(DataType.DateTime)]
        public DateTime DateUpdated { set; get; }
        //
        //
        public string Picture { get; set; }
        //
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileUserId { set; get; }
        //
        public virtual ICollection<Tag> Tags { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
        //
        //
        public int PostTypeID { set; get; }
        public virtual PostType PostType { set; get; }
        
    }
}