﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Tag
    {
        public int TagID { set; get; }

        [Required(ErrorMessage = "Phải được nhập nội dung")]
        [StringLength(100, ErrorMessage = "Số lượng ký tự trong khoảng 3 đến 100 ký tự", MinimumLength = 3)]
        public String Content { set; get; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}