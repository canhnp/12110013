﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web_Final.Models
{
    public class Contact1
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [StringLength(50, ErrorMessage = "Số lượng ký tự trong khoảng 20 - 50", MinimumLength = 20)]
        public String Title { set; get; }
        [Required(ErrorMessage = "Phải nhập nội dung")]
        [RegularExpression(@"^.{50,}$", ErrorMessage = "Nhập tối thiểu 50 ký tự")]
        public String Body { set; get; }
        [DataType(DataType.EmailAddress)]
        public String EmailNguoiGui { set; get; }
        //
        public String EmailNguoiNhan { set; get; }
        //
        public virtual UserProfile UserProfile { set; get; }
        public int UserProfileID { set; get; }
    }
}