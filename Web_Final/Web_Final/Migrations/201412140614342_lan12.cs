namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan12 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contacts", "UserProfile_UserId", "dbo.UserProfile");
            DropIndex("dbo.Contacts", new[] { "UserProfile_UserId" });
            DropTable("dbo.Contacts");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        Email = c.String(),
                        UserProfileID = c.Int(nullable: false),
                        UserProfile_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateIndex("dbo.Contacts", "UserProfile_UserId");
            AddForeignKey("dbo.Contacts", "UserProfile_UserId", "dbo.UserProfile", "UserId");
        }
    }
}
