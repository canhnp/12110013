namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PostTypes",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        NameType = c.String(),
                        AdminFlag = c.String(),
                        RelationshipFlag = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Posts", "PostTypeID", c => c.Int(nullable: false));
            AddForeignKey("dbo.Posts", "PostTypeID", "dbo.PostTypes", "ID", cascadeDelete: true);
            CreateIndex("dbo.Posts", "PostTypeID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "PostTypeID" });
            DropForeignKey("dbo.Posts", "PostTypeID", "dbo.PostTypes");
            DropColumn("dbo.Posts", "PostTypeID");
            DropTable("dbo.PostTypes");
        }
    }
}
