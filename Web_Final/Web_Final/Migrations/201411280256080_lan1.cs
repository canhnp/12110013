namespace Web_Final.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserProfile",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateUpdated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Posts", new[] { "UserProfileUserId" });
            DropForeignKey("dbo.Posts", "UserProfileUserId", "dbo.UserProfile");
            DropTable("dbo.Posts");
            DropTable("dbo.UserProfile");
        }
    }
}
