﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Blog2.Models
{
    [Table("BaiViet")]
    public class Post
    {
        public int ID { set; get; }
        [Required]
        public String Title { set; get; }
        [StringLength(250,ErrorMessage="Số lượng ký tự trong khoảng 10 - 250",MinimumLength=10)]
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }
        public DateTime DateUpdated { set; get; }
        //
        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}